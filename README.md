# Simples exmplo de extensão do Chrome

![](/128.png)

### Arquivos
+ <https://gitlab.com/diaslasd/exola/-/blob/master/manifest.json>
+ <https://gitlab.com/diaslasd/exola/-/blob/master/ola.html>
+ <https://gitlab.com/diaslasd/exola/-/blob/master/icon.png>
+ <https://gitlab.com/diaslasd/exola/-/blob/master/128.png>

### Instalação
1. salve esses 4 arquivos em uma pasta
2. No Chrome, abra "Extensões" em "Mais Ferramentas"
3. Ative o "Modo do desenvolvedor"
4. Abra a pasta criada em "Carregar sem compactação"


### Documentação
[Extensions](https://developer.chrome.com/docs/extensions/)


